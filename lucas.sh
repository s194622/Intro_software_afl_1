#!/bin/bash

# Programmet tager et heltalsargument og printer lucas-sekvensen til og med argumentets værdi.

n1=2
n2=1
n3=0

# Tjekker om inputtet er 0 eller mindre. Giver intet resultat i dette tilfælde.
# For hvis argumentet ikke er positivt, findes der ingen gyldig lucas-sekvens.
if [ $1 -le 0 ]
then
    :

# Tjekker om inputtet er 1. Giver det tilsvarende lucastal.
elif [ $1 -eq 1 ]
then
    echo 1

# Ellers finder den lucastallene til den givne n-værdi.
# Først printer vi de to forudberegnede tal fra sekvensen, vi tidligere har defineret som variabler.
else
    echo $n1
    echo $n2

# Efter finder den resten af lucastallene, gennem følgende loop.
# Her benytter vi summen af de forrige to tal. Hvis summen er større en vores input, stopper loopen.
while [ $(bc <<< $n1+$n2) -le $1 ]
do
    n3=$(bc <<< $n1+$n2)
    echo $n3
    n1=$n2
    n2=$n3
done
fi
